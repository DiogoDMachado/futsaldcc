<?php

$game_input = $_REQUEST["game"];

$dir = 'sqlite:db/fute_db.sqlite';
try{
	$dbh  = new PDO($dir) or die("cannot open the database");
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e){
	$errorMsg = $e->getMessage();
	echo $errorMsg;
	return;
}

session_start();
$old_sessionID = session_id();
$query_sess =  "SELECT name FROM Player WHERE session_id = '".$old_sessionID."'";

$result= NULL;

try{
	$res = $dbh->query($query_sess);
	if($res != NULL){
		$f = $res->fetch();
		$result = $f['name'];
	}
} catch(PDOException $e){
        $errorMsg = $e->getMessage();
		echo $errorMsg;
		$res = NULL;
        return;
}
$res = NULL;

if($game_input!=NULL){
	try{
		if($result){
			$query = "SELECT name, type, participates, timestamp FROM Player NATURAL JOIN Plays WHERE Player.name = Plays.player_name and Plays.game_id = :game_input ORDER BY Player.type desc, Plays.timestamp";
			$res = $dbh->prepare($query);
			$res->bindParam(':game_input', $game_input);       
			$res->execute();
			//$res = $dbh->query($query);
			if($res!=NULL){
				foreach ($res as $row){
					$reply = "\n".$row[0]." , ".$row[1]." , ".$row[2];
					$timestamp = $row[3];
					$query = "SELECT date FROM Game Where id = :game_input";
					$res_date = $dbh->prepare($query);
					$res_date->bindParam(':game_input', $game_input);       
					$res_date->execute();
					//$res_date = $dbh->query($query);
					$redline = "";
					if($res_date!=NULL){
						$f = $res_date->fetch();
						$result_date = $f['date'];
		
						$redline = $result_date;
						$day = substr($redline, 8, 2);
						$month = substr($redline, 5, 2);
						$month_before = intval($month)-1;
						$year = substr($redline, 0, 4);
						$number_days_month = cal_days_in_month(CAL_GREGORIAN, $month_before, intval($year)); 
					
						$int_day = intval($day);
						$int_day = $int_day-2;
						if($int_day<0){
							$int_day = $int_day%$number_days_month;
							if($int_day<10){
								$day = "0".strval($int_day);
							}else{
								$day = strval($int_day);
							}
							$redline=substr_replace($redline, $month_before."-".$day, 5, 5);
						}
						else{
							if($int_day<10){
								$day = "0".strval($int_day);
							}else{
								$day = strval($int_day);
							}
							$redline=substr_replace($redline, $day, 8, 2);		
						}
						if($redline != ""){
							//echo "\n redline: ".$redline;
							$query = "SELECT Plays.timestamp FROM Plays natural join Player WHERE Player.name = Plays.player_name and Plays.game_id = :game_input and Player.name = :p_name";
							$res_late = $dbh->prepare($query);
							$res_late->bindParam(':game_input', $game_input);  
							$res_late->bindParam(':p_name', $row[0]);  
							$res_late->execute();
							
							//$res_late = $dbh->query($query);
							//echo $reply;
							//echo "\n red: ".$redline."\n";
							if($res_late!=NULL){
								foreach ($res_late as $row){
									if($row !=NULL){
										if($row[0]<$redline){
											$reply = $reply." , -1";
										}else{
											$reply = $reply." , 1";
										}
									}
								}
							}else{
								$reply = $reply." , -1";
							}
						}
					}	
					$reply = $reply." , ".$timestamp;
					echo $reply;
				}
				$res = null;
				$dbh = null;
				return;
			}
		}
	} catch(PDOException $e){
		$errorMsg = $e->getMessage();
		echo $errorMsg;
		return;
	}
}else{
	try{
		if($result){
			$query = "SELECT Game.id FROM Game WHERE Game.date > date('now') order by date";
			$res = $dbh->query($query);
			if($res!=NULL){
				foreach ($res as $row){
					echo "\n".$row[0];
				}
				return;
			}
		}
	} catch(PDOException $e){
		$errorMsg = $e->getMessage();
		echo $errorMsg;
		return;
	}
}
$res = null;
$dbh = null;
echo -1;
?>

