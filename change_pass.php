<?php

$pass_input = $_REQUEST["pass"];


$dir = 'sqlite:db/fute_db.sqlite';
try{
	$dbh  = new PDO($dir) or die("cannot open the database");
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e){
	$errorMsg = $e->getMessage();
	echo $errorMsg;
	return;
}
	
session_start();
$old_sessionID = session_id();
$query_sess =  "SELECT name FROM Player WHERE session_id = '".$old_sessionID."'";

$result= NULL;

try{
	$res = $dbh->query($query_sess);
	if($res != NULL){
		$f = $res->fetch();
		$result = $f['name'];
	}
} catch(PDOException $e){
        $errorMsg = $e->getMessage();
		echo $errorMsg;
		$res = NULL;
        return;
}
$res = NULL;

if($result){
			
	$res = NULL;
	$query_write =  "UPDATE Player SET pass = :pass WHERE name = :name";
	$stmt = $dbh->prepare($query_write);
	$stmt->bindParam(':pass', $pass_input);       
	$stmt->bindParam(':name', $result);
	$stmt->execute();
	echo 1;
	$stmt = NULL;
	return;
}
$res = NULL;
echo -1;

?>