<?php

$game_id = $_REQUEST["id"];

$dir = 'sqlite:db/fute_db.sqlite';

try{
	$dbh  = new PDO($dir) or die("cannot open the database");
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e){
	$errorMsg = $e->getMessage();
	echo $errorMsg;
	return;
}

session_start();
$old_sessionID = session_id();
$query_sess =  "SELECT admin FROM Player WHERE session_id = '".$old_sessionID."'";

$result= NULL;

try{
	$res = $dbh->query($query_sess);
	if($res != NULL){
		$f = $res->fetch();
		$result = $f['admin'];
	}
} catch(PDOException $e){
        $errorMsg = $e->getMessage();
		echo $errorMsg;
		$res = NULL;
        return;
}

$res = NULL;

try{

	if($result==="1"){
		$query = "DELETE FROM Game WHERE id = :game_id";
		$stmt = $dbh->prepare($query);
		$stmt->bindParam(':game_id', $game_id);
		$stmt->execute();
		//$dbh->exec($query);
		//$res = $dbh->query($query);
		$dbh = NULL;
		echo 1;
		return;
	}
} catch(PDOException $e){
	$errorMsg = $e->getMessage();
	echo $errorMsg;
	return;
}

echo -1;
?>

