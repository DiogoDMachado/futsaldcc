function next(event){
	if(event.keyCode === 13){
		document.getElementById("password").focus();	
	}
}

function enter(event){
	if(event.keyCode === 13){
		document.getElementById("enterLogin").click();
	}
}

function getTime(){
	var date = new Date();
	
	var year = date.getFullYear();
	var month = date.getMonth()+1;
	var day = date.getDate();
	var hour = date.getHours();
	var minutes = date.getMinutes();
	
	return year+"-"+month+"-"+day+" "+hour+":"+minutes;
}

function logout(){
		var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
				//alert("sessON"+this.responseText+" :");
				if(this.responseText==1){
					document.getElementById("login").style.visibility="visible";
					document.getElementById("maincontent").style.visibility="hidden";
					document.body.style.background = "url('images/main.jpg') no-repeat center center fixed";
					document.body.style.backgroundSize = "100% 100%";
					changetab(null,'maincontent_tab');
					
				}
            }
        };
        xmlhttp.open("GET", "http://www.alunos.dcc.fc.up.pt/~up200803609/fute-10/logout.php?_="+ getTime(), true);
        xmlhttp.send();	
}

function sessionOn(){
	document.body.style.background = "url('images/main.jpg') no-repeat center center fixed";
	document.body.style.backgroundSize = "100% 100%";
	
	var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
				//alert("sessON"+this.responseText+" :");
				if(this.responseText==1){
					document.getElementById("login").style.visibility="hidden";
					document.getElementById("maincontent").style.visibility="visible";
					document.body.style.background = "url('images/login.jpg') no-repeat center center fixed";
					document.body.style.backgroundSize = "100% 100%";
					changetab(null,'maincontent_tab');
					
				}
            }
        };
        xmlhttp.open("GET", "http://www.alunos.dcc.fc.up.pt/~up200803609/fute-10/login.php?_="+ getTime(), true);
        xmlhttp.send();	
}

function login(){
	var usernameT = document.getElementById("username");
	var passwordT = document.getElementById("password");
	
	var user = usernameT.value;
	var pass = passwordT.value;

	validate(user,pass);
}

function validate(name, pass){

	var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
				//alert(this.responseText);
				if(this.responseText==1){
					document.getElementById("login").style.visibility="hidden";
					document.getElementById("maincontent").style.visibility="visible";
					document.body.style.background = "url('images/login.jpg') no-repeat center center fixed";
					document.body.style.backgroundSize = "100% 100%";
					changetab(null,'maincontent_tab');
					//getGames();
				}
            }
        };
        xmlhttp.open("GET", "http://www.alunos.dcc.fc.up.pt/~up200803609/fute-10/login.php?_="+ getTime()+"&name="+name+"&pass="+pass, true);
        xmlhttp.send();	
		
}





function fill_table(line){
	
	if(line && line != ""){
	var master_tbl_h = document.createElement('td');
	var wrapper = document.createElement('div');
	
	var rowV = line.split(",");
	var game_id = rowV[0].replace(/\s/g, "");;
	var date = rowV[1];
	var day = rowV[2];
	var field = rowV[3];
	wrapper.id=game_id;
	
	var h = document.createElement("H3") 
	var t = document.createTextNode("GAME N"+'\xB0'+""+game_id+": "+date+" ["+day+"]");     
	h.appendChild(t); 
	wrapper.appendChild(h);
	
	h = document.createElement("H3") 
	t = document.createTextNode("AT: "+field);     
	h.appendChild(t); 
	wrapper.appendChild(h);
	
	var tbl = document.createElement('table');
	//tbl.style.width = '100%';
	tbl.setAttribute('border', '1');
	
	var tblr = document.createElement('tr');
	var tblh = document.createElement('th');
	tblh.innerHTML = "Nickname";
	tblr.appendChild(tblh);
	
	tblh = document.createElement('th');
	tblh.innerHTML = "Type";
	tblr.appendChild(tblh);
	
	tblh = document.createElement('th');
	tblh.innerHTML = "Availability";
	tblr.appendChild(tblh);
	
	tbl.appendChild(tblr);
	
	var tbdy = document.createElement('tbody');
	tbdy.id = "tabela_game"+game_id;

	tbl.appendChild(tbdy);
	
	wrapper.appendChild(tbl);
	
	var button = document.createElement("input");
	button.type = "button";
	button.value = "CONFIRM!";
	button.onclick = function () { confirmar(1, this.parentNode.id); };
	wrapper.appendChild(button);
	
	button = document.createElement("input");
	button.type = "button";
	button.value = "CAN'T GO!";
	button.onclick = function () { confirmar(-1, this.parentNode.id); };
	wrapper.appendChild(button);
	master_tbl_h.appendChild(wrapper);
	
	return master_tbl_h;
	}
}

function getGames(){
	
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			//alert(this.responseText);
			var body = document.getElementById("matches_cont");
			
			var master_div = document.createElement('div');
			master_div.id="matches_cont";
			var master_tbl = document.createElement('table');
			master_tbl.id="master_tblr";
			//master_tbl.style.width = '100%';
			
			
			var allText = this.responseText;
			var lines = allText.split('\n');
		

			for (var i = 1; i < lines.length; i+=2) {
				
				var master_tbl_r = document.createElement('tr');
				
				var cell = fill_table(lines[i]);
				var cell2 = fill_table(lines[i+1]);
				
				master_tbl_r.appendChild(cell);
				if(cell2){
					master_tbl_r.appendChild(cell2);	
				}
				
				
				master_tbl.appendChild(master_tbl_r);
				
				
				//body.appendChild(wrapper);
				//fillTable(game_id);
			}
			master_div.appendChild(master_tbl);
			button = document.createElement("input");
			button.type = "button";
			button.value = "REFRESH TABLES!";
			button.onclick = function () { fillTables(); };
			master_div.appendChild(button);
			body.parentNode.replaceChild(master_div, body);
			fillTables();
		}
	};
	xmlhttp.open("GET", "http://www.alunos.dcc.fc.up.pt/~up200803609/fute-10/games.php?_=" + getTime(), true);
	xmlhttp.send();
}


function fillTable(nextgame){//game1 / game2

	//alert("fill: "+nextgame);
	var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
				//alert(this.responseText);
				var allText = this.responseText;
				var lines = allText.split('\n');
				var old_tbody = document.getElementById("tabela_game"+nextgame);
				//alert("tabela_game"+nextgame);

				var tbdy = document.createElement('tbody');
				for (var i = 1; i < lines.length; i++) {
					
					var tr = document.createElement('tr');
					
					var rowV = lines[i].split(",");
					var name = rowV[0].replace(/\s/g, "");
					var type = rowV[1];
					var conf = rowV[2].replace(/\s/g, "");
					var late = rowV[3].replace(/\s/g, "");
					//alert(late);
					if(conf==="1"){
						conf="I'm GOING!";
					}else{
						conf="NOPS...";
					}
					
					var td = document.createElement('td');
					var nameLabel = document.createTextNode(name);
					if(late=="1"){
						td.style.color = "red"
					}else{
						td.style.color = "black"
					}
					td.appendChild(nameLabel);
					tr.appendChild(td);
					
					td = document.createElement('td');
					td.appendChild(document.createTextNode(type));
					tr.appendChild(td);
					
					td = document.createElement('td');
					td.appendChild(document.createTextNode(conf));
					tr.appendChild(td);
					
					tbdy.appendChild(tr);
				}
				tbdy.id = "tabela_game"+nextgame;
				old_tbody.parentNode.replaceChild(tbdy, old_tbody);
				
            }
        };
        xmlhttp.open("GET", "http://www.alunos.dcc.fc.up.pt/~up200803609/fute-10/status.php?_=" + getTime()+"&game="+nextgame, true);
        xmlhttp.send();	
}


function fillTables(){//game1 / game2


	var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
				var allText = this.responseText;
				var lines = allText.split('\n');
				for (var i = 1; i < lines.length; i++) {

					var rowV = lines[i].split(",");
					var game_id = rowV[0].replace(/\s/g, "");
					fillTable(game_id);
				}
	
            }
        };
        xmlhttp.open("GET", "http://www.alunos.dcc.fc.up.pt/~up200803609/fute-10/status.php?_=" + getTime(), true);
        xmlhttp.send();	
}



function confirmar(number,game_id){


	var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
				fillTable(game_id);
            }
        };
		var url = "http://www.alunos.dcc.fc.up.pt/~up200803609/fute-10/confirm.php?_="+ getTime()+"&game_id="+game_id+"&conf="+number;
        xmlhttp.open("GET", url, true);
        xmlhttp.send();	
}

function getDebts(){
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			//alert(this.responseText);
			var body = document.getElementById("debt_cont");
			
			var master_div = document.createElement('div');
			master_div.id="debt_cont";
			var master_tbl = document.createElement('table');
			master_tbl.id="master_debt_tblr";
			//master_tbl.style.width = '100%';
			master_tbl.style.width = '100%';
			master_tbl.setAttribute('border', '1');
			
			
			var allText = this.responseText;
			var lines = allText.split('\n');

			for (var i = 1; i < lines.length; i++) {//name, debt
				
				var master_tbl_r = document.createElement('tr');

				var rowV = lines[i].split(",");
				
				var name = rowV[0].replace(/\s/g, "");
				var debt = rowV[1].replace(/\s/g, "");
				if(debt===""){
					debt="0";
				}
				
				var td = document.createElement('td');
				td.appendChild(document.createTextNode(name));
				master_tbl_r.appendChild(td);
				
				td = document.createElement('td');
				td.appendChild(document.createTextNode(debt+" euroballs"));
				master_tbl_r.appendChild(td);
				
				master_tbl.appendChild(master_tbl_r);

			}
			master_div.appendChild(master_tbl);
			body.parentNode.replaceChild(master_div, body);

		}
	};
	xmlhttp.open("GET", "http://www.alunos.dcc.fc.up.pt/~up200803609/fute-10/debts.php?_=" + getTime(), true);
	xmlhttp.send();
}

function change_pass() {
	var pass_input = document.getElementById("pass_input").value;
	if(pass_input!=""){
		var test = pass_input.search(/^[a-z0-9]+$/i);
		if(test===0){
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				//alert(this.responseText);
				if(this.responseText==="1"){
					var label = document.getElementById("passErrorL");
					label.value = "Password changed successfully!";
					label.style.color = 'green';
				}else{
					var label = document.getElementById("passErrorL");
					label.value = "Error changing Password!";
					label.style.color = 'red';
				}
			};
			xmlhttp.open("GET", "http://www.alunos.dcc.fc.up.pt/~up200803609/fute-10/change_pass.php?_=" + getTime()+"&pass="+pass_input, true);
			xmlhttp.send();
		}
	}
}

function change_email(){
	var email_input = document.getElementById("email_input").value;
	if(email_input!=""){
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		var test = re.test(String(email_input).toLowerCase());
		if(test===true){
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				if(this.responseText==="1"){
					var label = document.getElementById("emailErrorL");
					label.value = "Email changed successfully!";
					label.style.color = 'green';
				}else{
					var label = document.getElementById("emailErrorL");
					label.value = "Error changing Email!";
					label.style.color = 'red';
				}
			};
			xmlhttp.open("GET", "http://www.alunos.dcc.fc.up.pt/~up200803609/fute-10/change_mail.php?_=" + getTime()+"&email="+email_input, true);
			xmlhttp.send();
		}
	}
}

function create_game(){
	
	var id_input = document.getElementById("field_id_input").value;
	var day_input = document.getElementById("field_day_input").value;
	var date_input = document.getElementById("field_date_input").value;
	
	if(id_input!="" && day_input!="" && date_input!=""){
		var test1 = id_input.search(/^[a-z0-9]+$/i);
		var test2 = day_input.search(/^[a-z0-9]+$/i);
		//var test3 = date_input.search(/^[0-9-:]+$/i);
		//2018-03-21 19:00
		if(test1===0 && test2===0 ){//&&test3===0){
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				//alert(this.responseText);
				if(this.responseText==="1"){
					var label = document.getElementById("create_game_error_Label");
					label.value = "Created game successfully!";
					label.style.color = 'green';
				}else{
					var label = document.getElementById("create_game_error_Label");
					label.value = "Error Creating Game!";
					label.style.color = 'red';
					//alert("nop");
				}
			};
			xmlhttp.open("GET", "http://www.alunos.dcc.fc.up.pt/~up200803609/fute-10/create_game.php?_=" + getTime()+"&id="+id_input+"&day="+day_input+"&date="+date_input, true);
			xmlhttp.send();
			//alert("http://www.alunos.dcc.fc.up.pt/~up200803609/fute-10/create_game.php?_=" + new Date().getTime()+"&id="+id_input+"&day="+day_input+"&date='"+date_input+"'");
		}
	}
}

function delete_game(){
	
	var delete_id_input = document.getElementById("delete_game_input").value;
	
	delete_id_input = delete_id_input.replace(/\s/g, ''); 
	
	if(delete_id_input!=""){
		var test1 = delete_id_input.search(/^[a-z0-9]+$/i);

		if(test1===0){
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				if(this.responseText==="1"){
					var label = document.getElementById("fieldDeleteErrorL_");
					label.value = "Deleted game successfully!";
					label.style.color = 'green';
				}else{
					var label = document.getElementById("fieldDeleteErrorL_");
					label.value = "Error deleting Game!";
					label.style.color = 'red';
				}
			};
			var url = "http://www.alunos.dcc.fc.up.pt/~up200803609/fute-10/delete_game.php?_=" + getTime()+"&id="+delete_id_input;
			xmlhttp.open("GET", url, true);
			xmlhttp.send();
		}
	}
}

function add_new_player(){
	var add_name_input = document.getElementById("add_player_input").value;
	var add_pass_input = document.getElementById("add_pass_input").value;
	var add_email_input = document.getElementById("add_email_input").value;
	
	if(add_name_input!="" && add_pass_input!=""){
		var test1 = add_name_input.search(/^[a-z0-9]+$/i);
		var test2 = add_pass_input.search(/^[a-z0-9]+$/i);
		var test3 = 0;
		
		if(add_email_input!=""){
			test3 = add_name_input.search(/^[a-z0-9]+$/i);
		}

		if(test1===0 && test2===0 &&test3===0){
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				if(this.responseText==="1"){
					var label = document.getElementById("add_player_error_Label");
					label.value = "Added new Player successfully!";
					label.style.color = 'green';
				}else{
					var label = document.getElementById("add_player_error_Label");
					label.value = "Error adding Player!";
					label.style.color = 'red';
				}
			};
			xmlhttp.open("GET", "http://www.alunos.dcc.fc.up.pt/~up200803609/fute-10/create_player.php?_=" + getTime()+"&name="+add_name_input+"&pass="+add_pass_input+"&email="+add_email_input, true);
			xmlhttp.send();
		}
	}
}
function delete_player(){
	var delete_player_input = document.getElementById("delete_player_name").value;
	
	if(delete_player_input!=""){
		var test1 = delete_player_input.search(/^[a-z0-9]+$/i);

		if(test1===0){
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				if(this.responseText==="1"){
					var label = document.getElementById("player_delete_error_Label_");
					label.value = "Deleted Player successfully!";
					label.style.color = 'green';
				}else{
					var label = document.getElementById("player_delete_error_Label_");
					label.value = "Error deleting Player!";
					label.style.color = 'red';
				}
			};
			xmlhttp.open("GET", "http://www.alunos.dcc.fc.up.pt/~up200803609/fute-10/delete_player.php?_=" + getTime()+"&name="+delete_player_input, true);
			xmlhttp.send();
		}
	}
}

function setUserpage() {
	
	// var body = document.getElementById("user_cont");
	// var master_div = document.createElement('div');
	// master_div.id = "user_cont";
	
	// var change_pass_div = document.createElement('div');
	
	//change pass------------------------
	
	// var pass_input = document.createElement("INPUT");
	// pass_input.id = "pass_input";
    // pass_input.setAttribute("type", "text");
    // pass_input.setAttribute("value", "New Password");
	// pass_input.onfocus = function(){this.value='';};
	
	
	// var passlabel = document.createElement("Label");
	// passlabel.setAttribute("for",pass_input);
	// passlabel.innerHTML = "Change Password: ";
	
	// var errorLabel = document.createElement("Label");
	// errorLabel.id = "passErrorL";
	
	// change_pass_div.appendChild(passlabel);
	// change_pass_div.appendChild(pass_input);
	// change_pass_div.appendChild(errorLabel);
	
	// var submit_pass = document.createElement("input");
	// submit_pass.type = "button";
	// submit_pass.value = "Submit New Pass!";
	// submit_pass.onclick = function () { change_pass(); };
	// change_pass_div.appendChild(submit_pass);

    // master_div.appendChild(change_pass_div);
	
	//change email---------------------------
	
	// var change_email_div = document.createElement('div');
	
	// var email_input = document.createElement("INPUT");
	// email_input.id = "email_input";
    // email_input.setAttribute("type", "email");
    // email_input.setAttribute("value", "New Email");
	// email_input.onfocus = function(){this.value='';};
	
	
	// var emaillabel = document.createElement("Label");
	// emaillabel.setAttribute("for",email_input);
	// emaillabel.innerHTML = "Change Email: ";
	
	// var email_error_Label = document.createElement("Label");
	// email_error_Label.id = "emailErrorL";
	
	// change_email_div.appendChild(emaillabel);
	// change_email_div.appendChild(email_input);
	// change_email_div.appendChild(email_error_Label);
	
	// var submit_email = document.createElement("input");
	// submit_email.type = "button";
	// submit_email.value = "Submit New Email!";
	// submit_email.onclick = function () { change_email(); };
	// change_email_div.appendChild(submit_email);

    // master_div.appendChild(change_email_div);
	
	
	// var xmlhttp = new XMLHttpRequest();
	// xmlhttp.onreadystatechange = function() {
		// if (this.readyState == 4 && this.status == 200) {
			//alert("resp: "+this.responseText);
			// if(this.responseText=="1"){
				
				// var change_admin_opt_div = document.createElement('div');
				// change_admin_opt_div.id = "admin_opt";
	
				// var h = document.createElement("H3") 
				// var t = document.createTextNode("Admin options:");     
				// h.appendChild(t); 
				// change_admin_opt_div.appendChild(h);
			
				
				//        CREATE GAME
				// var create_game_div = document.createElement('div');
				
				// h = document.createElement("H3") 
				// t = document.createTextNode("Create game:");   
				// h.appendChild(t); 
				// create_game_div.appendChild(h);
				
				
				//FIELD id
				// var wrap = document.createElement('div');
				// var campo_id_input = document.createElement("INPUT");
				// campo_id_input.id = "field_id_input";
				// campo_id_input.setAttribute("type", "text");
				// campo_id_input.setAttribute("value", "FIELD ID");
				// campo_id_input.onfocus = function(){this.value='';};
				
				
				// var campo_id_label = document.createElement("Label");
				// campo_id_label.setAttribute("for",campo_id_input);
				// campo_id_label.innerHTML = "Id Field: ";
				
				// var fieldID_error_Label = document.createElement("Label");
				// fieldID_error_Label.id = "fieldIDErrorL";
	
				
				// wrap.appendChild(campo_id_label);
				// wrap.appendChild(campo_id_input);
				// wrap.appendChild(fieldID_error_Label);
				// create_game_div.appendChild(wrap);
				
				//FIELD day
				// wrap = document.createElement('div');
				// var campo_day_input = document.createElement("INPUT");
				// campo_day_input.id = "field_day_input";
				// campo_day_input.setAttribute("type", "text");
				// campo_day_input.setAttribute("value", "GAME DAY");
				// campo_day_input.onfocus = function(){this.value='';};
				
				
				// var campo_day_label = document.createElement("Label");
				// campo_day_label.setAttribute("for",campo_day_input);
				// campo_day_label.innerHTML = "Game Day: ";
				
				// var fieldDay_error_Label = document.createElement("Label");
				// fieldDay_error_Label.id = "fieldDAYErrorL";
				
				// wrap.appendChild(campo_day_label);
				// wrap.appendChild(campo_day_input);
				// wrap.appendChild(fieldDay_error_Label);
				// create_game_div.appendChild(wrap);
				
				//FIELD date
				// wrap = document.createElement('div');
				// var campo_date_input = document.createElement("INPUT");
				// campo_date_input.id = "field_date_input";
				// campo_date_input.setAttribute("type", "date");
				// campo_date_input.setAttribute("value", "GAME DATE");
				// campo_date_input.onfocus = function(){this.value='';};
				
				
				// var campo_id_label = document.createElement("Label");
				// campo_id_label.setAttribute("for",campo_date_input);
				// campo_id_label.innerHTML = "Game Date [YYYY-MM-DD]: ";
				
				// var fieldDate_error_Label = document.createElement("Label");
				// fieldDate_error_Label.id = "fieldDATEErrorL";
				
				// wrap.appendChild(campo_id_label);
				// wrap.appendChild(campo_date_input);
				// wrap.appendChild(fieldDate_error_Label);
				// create_game_div.appendChild(wrap);
				
				//TIME
				// wrap = document.createElement('div');
				// var campo_time_input = document.createElement("INPUT");
				// campo_time_input.id = "field_time_input";
				// campo_time_input.setAttribute("type", "time");
				// campo_time_input.setAttribute("value", "GAME DATE");
				// campo_time_input.onfocus = function(){this.value='';};
				
				
				// var campo_id_label = document.createElement("Label");
				// campo_id_label.setAttribute("for",campo_time_input);
				// campo_id_label.innerHTML = "Game Time [H:M]: ";
				
				// var fieldTime_error_Label = document.createElement("Label");
				// fieldTime_error_Label.id = "fieldTimeErrorL";
				
				// wrap.appendChild(campo_id_label);
				// wrap.appendChild(campo_time_input);
				// wrap.appendChild(fieldTime_error_Label);
				// create_game_div.appendChild(wrap);
				
				
				
				// wrap = document.createElement('div');
				// var submit_game = document.createElement("input");
				// submit_game.type = "button";
				// submit_game.value = "Create New Game!";
				// submit_game.onclick = function () { create_game(); };
				
				// var create_game_error_Label = document.createElement("Label");
				// create_game_error_Label.id = "create_game_error_Label";
				
				// wrap.appendChild(submit_game);
				// wrap.appendChild(create_game_error_Label);
				// create_game_div.appendChild(wrap);
				
				// change_admin_opt_div.appendChild(create_game_div);
				
				
				//        DELETE GAME
				// var delete_game_div = document.createElement('div');
				// h = document.createElement("H3") 
				// t = document.createTextNode("Delete game:");   
				// h.appendChild(t); 
				// delete_game_div.appendChild(h);
			
			
				// var delete_game_input = document.createElement("INPUT");
				// delete_game_input.id = "delete_game_input";
				// delete_game_input.setAttribute("type", "text");
				// delete_game_input.setAttribute("value", "GAME ID");
				// delete_game_input.onfocus = function(){this.value='';};
				
				
				// var delete_game_label = document.createElement("Label");
				// delete_game_label.setAttribute("for",delete_game_input);
				// delete_game_label.innerHTML = "Delete Game By Id: ";
				
				// var delete_game_error_Label = document.createElement("Label");
				// delete_game_error_Label.id = "fieldDeleteErrorL";
				
				// delete_game_div.appendChild(delete_game_label);
				// delete_game_div.appendChild(delete_game_input);
				// delete_game_div.appendChild(delete_game_error_Label);
				
				// var delete_game_in = document.createElement("input");
				// delete_game_in.type = "button";
				// delete_game_in.value = "Delete Game!";
				// delete_game_in.onclick = function () { delete_game(); };
				// delete_game_div.appendChild(delete_game_in);
				
				// var delete_game_error_Label_ = document.createElement("Label");
				// delete_game_error_Label_.id = "fieldDeleteErrorL_";
				
				// delete_game_div.appendChild(delete_game_error_Label_);

				// change_admin_opt_div.appendChild(delete_game_div);
				
				
				//        ADD PLAYER
				// var add_player_div = document.createElement('div');
				// h = document.createElement("H3") 
				// t = document.createTextNode("Add Player:");   
				// h.appendChild(t); 
				// add_player_div.appendChild(h);
				//name pass debt type session_id admin email
			
				//name
				// var wrap = document.createElement('div');
				// var player_name_input = document.createElement("INPUT");
				// player_name_input.id = "add_player_input";
				// player_name_input.setAttribute("type", "text");
				// player_name_input.setAttribute("value", "GAME DATE");
				// player_name_input.onfocus = function(){this.value='';};
				
				
				// var player_name_label = document.createElement("Label");
				// player_name_label.setAttribute("for",player_name_input);
				// player_name_label.innerHTML = "[New Player] Name: ";
				
				// var player_name_error_Label = document.createElement("Label");
				// player_name_error_Label.id = "playerNameErrorL";
				
				// wrap.appendChild(player_name_label);
				// wrap.appendChild(player_name_input);
				// wrap.appendChild(player_name_error_Label);
				// add_player_div.appendChild(wrap);
				
				
				//pass
				// var wrap = document.createElement('div');
				// var player_pass_input = document.createElement("INPUT");
				// player_pass_input.id = "add_pass_input";
				// player_pass_input.setAttribute("type", "password");
				// player_pass_input.setAttribute("value", "GAME DATE");
				// player_pass_input.onfocus = function(){this.value='';};
				
				
				// var player_pass_label = document.createElement("Label");
				// player_pass_label.setAttribute("for",player_pass_input);
				// player_pass_label.innerHTML = "[New Player] Pass: ";
				
				// var player_pass_error_Label = document.createElement("Label");
				// player_pass_error_Label.id = "playerPassErrorL";
				
				// wrap.appendChild(player_pass_label);
				// wrap.appendChild(player_pass_input);
				// wrap.appendChild(player_pass_error_Label);
				
				// add_player_div.appendChild(wrap);
				
				
				//email
				// var wrap = document.createElement('div');
				// var player_email_input = document.createElement("INPUT");
				// player_email_input.id = "add_email_input";
				// player_email_input.setAttribute("type", "text");
				// player_email_input.setAttribute("value", "GAME DATE");
				// player_email_input.onfocus = function(){this.value='';};
				
				
				// var player_email_label = document.createElement("Label");
				// player_email_label.setAttribute("for",player_email_input);
				// player_email_label.innerHTML = "[New Player] Email: ";
				
				// var player_email_error_Label = document.createElement("Label");
				// player_email_error_Label.id = "playerEmailErrorL";
				
				// wrap.appendChild(player_email_label);
				// wrap.appendChild(player_email_input);
				// wrap.appendChild(player_email_error_Label);
				// add_player_div.appendChild(wrap);
			
			
				//submit button
				// var submit_add_player = document.createElement("input");
				// submit_add_player.type = "button";
				// submit_add_player.value = "Add Player!";
				// submit_add_player.onclick = function () { add_new_player(); };
				// add_player_div.appendChild(submit_add_player);
				
				// var add_player_error_Label = document.createElement("Label");
				// add_player_error_Label.id = "add_player_error_Label";

				// add_player_div.appendChild(add_player_error_Label);
				// change_admin_opt_div.appendChild(add_player_div);
				
				
				 //        DELETE PLAYER
				// var delete_player_div = document.createElement('div');
				// h = document.createElement("H3") 
				// t = document.createTextNode("Delete Player:");   
				// h.appendChild(t); 
				// delete_game_div.appendChild(h);
			
			
				// var delete_player_input = document.createElement("INPUT");
				// delete_player_input.id = "delete_player_name";
				// delete_player_input.setAttribute("type", "text");
				// delete_player_input.setAttribute("value", "Player Name");
				// delete_player_input.onfocus = function(){this.value='';};
				
				
				// var delete_player_label = document.createElement("Label");
				// delete_player_label.setAttribute("for",pass_input);
				// delete_player_label.innerHTML = "Delete Player by Name: ";
				
				// var player_delete_error_Label = document.createElement("Label");
				// player_delete_error_Label.id = "playerDeleteErrorL";
				
				// delete_game_div.appendChild(delete_player_label);
				// delete_game_div.appendChild(delete_player_input);
				// delete_game_div.appendChild(player_delete_error_Label);
				
				// var delete_player_submit = document.createElement("input");
				// delete_player_submit.type = "button";
				// delete_player_submit.value = "Delete Player!";
				// delete_player_submit.onclick = function () { delete_player(); };
				// delete_game_div.appendChild(delete_player_submit);
				
				// var player_delete_error_Label_ = document.createElement("Label");
				// player_delete_error_Label_.id = "player_delete_error_Label_";
				
				// delete_game_div.appendChild(player_delete_error_Label_);

				// change_admin_opt_div.appendChild(delete_game_div);
				
				
				// master_div.appendChild(change_admin_opt_div);
				
				//document.getElementById("admin_opt").style.display = "block";
				
			// }
		// }
	// };
	// xmlhttp.open("GET", "http://www.alunos.dcc.fc.up.pt/~up200803609/fute-10/admin.php?_=" + getTime(), true);
	// xmlhttp.send();
	
	// body.parentNode.replaceChild(master_div, body);
	
}

function changetab(evt, tab_name) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tab_name).style.display = "block";
	if(tab_name ==="maincontent_tab"){
		getGames();
	}if(tab_name ==="debts"){
		getDebts();
	}if(tab_name ==="userpage"){
		setUserpage();
	}
	if(evt !=null){
		evt.currentTarget.className += " active";
	}
}
