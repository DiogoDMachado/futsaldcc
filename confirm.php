<?php

$game_id = $_REQUEST["game_id"];
$conf = $_REQUEST["conf"];

date_default_timezone_set('Europe/Dublin');

$timestamp = date('Y-m-d H:i');//$_REQUEST["_"];

$dir = 'sqlite:db/fute_db.sqlite';
try{
	$dbh  = new PDO($dir) or die("cannot open the database");
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e){
	$errorMsg = $e->getMessage();
	echo $errorMsg;
	return;
}


session_start();
$old_sessionID = session_id();
$query_sess =  "SELECT name FROM Player WHERE session_id = '".$old_sessionID."'";

try{
	$res = $dbh->query($query_sess);
	$result= NULL;
	
	if($res != NULL){
		$f = $res->fetch();
		$result = $f['name'];
	}
} catch(PDOException $e){
        $errorMsg = $e->getMessage();
		echo $errorMsg;
		$res = NULL;
        return;
}
$res = NULL;

if($result){
	
	$name_ = $result;
	$query_select =  "Select * From Plays Where player_name = :name and game_id = :game_id";
	try{
		$res = $dbh->prepare($query_select);
		$res->bindParam(':name', $result);       
		$res->bindParam(':game_id', $game_id);
		$res->execute();
		//$res = $dbh->query($query_select);
		$result= NULL;
	
		if($res != NULL){
			$f = $res->fetch();
			$result = $f['player_name'];
		}
	} catch(PDOException $e){
        $errorMsg = $e->getMessage();
		echo $errorMsg;
		$res = NULL;
        return;
	}
	$res = NULL;
	
	if($result){//existe registo
		try{
			$query_update =  "UPDATE Plays SET participates = :conf WHERE player_name = :name and game_id = :game_id";
			//$query_update =  "UPDATE Plays SET participates = ".$conf." WHERE player_name = '".$name_."' and game_id = ".$game_id;
			
			$stmt = $dbh->prepare($query_update);
			$stmt->bindParam(':conf', $conf);       
			$stmt->bindParam(':name', $name_);
			$stmt->bindParam(':game_id', $game_id);
			$stmt->execute();
			$stmt = NULL;

			
			echo 1;
			return;
		} catch(PDOException $e){
			$errorMsg = $e->getMessage();
			echo $errorMsg;
			$res = NULL;
			return;
		}
	}else{
		$query_write =  "INSERT INTO Plays(player_name, game_id, participates, timestamp) VALUES (:name, :game_id, :conf, :timeStamp)";
		$stmt = $dbh->prepare($query_write);
		$stmt->execute(array(
			"name" => $name_,
			"conf" => $conf,
			"game_id" => $game_id,
			"timeStamp" => $timestamp
		));
		echo 1;
		$stmt = NULL;
		return;
	}
}else{
	echo -1;
}

?>
